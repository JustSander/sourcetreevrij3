using UnrealBuildTool;

public class Vrij3ToyShooterTarget : TargetRules
{
	public Vrij3ToyShooterTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Vrij3ToyShooter");
	}
}
